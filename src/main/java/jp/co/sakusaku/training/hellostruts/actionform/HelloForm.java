 package jp.co.sakusaku.training.hellostruts.actionform;

 import java.util.Date;

import org.apache.struts.action.ActionForm;

 public class HelloForm extends ActionForm {

 public Date now;

 public Date getNow() {
 return now;
 }

 public void setNow(Date now) {
 this.now = now;
 }

public String hellostruts;

public String getHellostruts(){
	return hellostruts;
}

public void setHellostruts(String hellostruts){
	this.hellostruts = hellostruts;
}
}