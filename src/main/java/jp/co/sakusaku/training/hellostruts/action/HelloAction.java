package jp.co.sakusaku.training.hellostruts.action;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sakusaku.training.hellostruts.actionform.HelloForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

 public class HelloAction extends Action { //HelloActionへActionを継承

 @Override
 public ActionForward execute(ActionMapping mapping, ActionForm form,
 HttpServletRequest req, HttpServletResponse resp) throws Exception {

 HelloForm helloForm = (HelloForm) form;//HelloFormをformにキャストしてHelloform型のhelloFormへ代入
 helloForm.now = new Date();
 req.setAttribute("form", helloForm); //formへhelloformを格納

 Calendar now = Calendar.getInstance(); //現在時間のインスタンス化

 int nt = now.get(Calendar.HOUR_OF_DAY); //ntは現在時刻。ここで○時を取得
 String hellostruts = ""; //hellostrutsを空白と定義

 if(nt >= 6 && nt < 12) { //ntは現在時刻。時間帯別でhellostrutsの表示を変えるif文
 hellostruts = "Good Morinig Struts";
 } else if(nt >= 12 && nt < 18) {
 hellostruts = "Good afternoon Struts";
 } else if(nt >= 18 && nt < 6) {
 hellostruts = "Good evening Struts";
 }
 helloForm.setHellostruts(hellostruts);

 return mapping.findForward("success");  //アクション・クラス実行後の移行先を指定。
 }
 }